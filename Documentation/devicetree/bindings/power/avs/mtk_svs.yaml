# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/power/avs/mtk_svs.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: Introduce SVS engine

maintainers:
  - Kevin Hilman <khilman@kernel.org>
  - Nishanth Menon <nm@ti.com>

description: |+
  The Smart Voltage Scaling(SVS) engine is a piece of hardware
  which has several controllers(banks) for calculating suitable
  voltage to different power domains(CPU/GPU/CCI) according to
  chip process corner, temperatures and other factors. Then DVFS
  driver could apply SVS bank voltage to PMIC/Buck.

properties:
  compatible:
    const: mediatek,mt8183-svs

  reg:
    description: Address range of the MTK SVS controller.
    maxItems: 1

  interrupts:
    description: IRQ for the MTK SVS controller.
    maxItems: 1

  clocks:
    description: Main clock for svs controller to work.

  clock-names:
    const: main

  nvmem-cells:
    maxItems: 2
    description:
      Phandle to the calibration data provided by a nvmem device.

  nvmem-cell-names:
    items:
      - const: svs-calibration-data
      - const: calibration-data

patternProperties:
  "^svs-(cpu-little|cpu-big|cci|gpu)$":
    type: object
    description:
      Each subnode represents one SVS bank.
        - svs-cpu-little (SVS bank device node of little CPU)
        - svs-cpu-big (SVS bank device node of big CPU)
        - svs-cci (SVS bank device node of CCI)
        - svs-gpu (SVS bank device node of GPU)

    properties:
      compatible:
       enum:
         - mediatek,mt8183-svs-cpu-little
         - mediatek,mt8183-svs-cpu-big
         - mediatek,mt8183-svs-cci
         - mediatek,mt8183-svs-gpu

      power-domains:
        description: Phandle to the associated power domain
        maxItems: 1

      operating-points-v2: true

      vcpu-little-supply:
        description: PMIC buck of little CPU

      vcpu-big-supply:
        description: PMIC buck of big CPU

      vcci-supply:
        description: PMIC buck of CCI

      vgpu-spply:
        description: PMIC buck of GPU

    required:
      - compatible
      - operating-points-v2

    additionalProperties: false

required:
  - compatible
  - reg
  - interrupts
  - clocks
  - clock-names
  - nvmem-cells
  - nvmem-cell-names

additionalProperties: false

examples:
  - |
    #include <dt-bindings/clock/mt8183-clk.h>
    #include <dt-bindings/interrupt-controller/arm-gic.h>
    #include <dt-bindings/interrupt-controller/irq.h>
    #include <dt-bindings/power/mt8183-power.h>

    svs: svs@1100b000 {
        compatible = "mediatek,mt8183-svs";
        reg = <0 0x1100b000 0 0x1000>;
        interrupts = <GIC_SPI 127 IRQ_TYPE_LEVEL_LOW>;
        clocks = <&infracfg CLK_INFRA_THERM>;
        clock-names = "main";
        nvmem-cells = <&svs_calibration>, <&thermal_calibration>;
        nvmem-cell-names = "svs-calibration-data", "calibration-data";

        svs_cpu_little: svs-cpu-little {
            compatible = "mediatek,mt8183-svs-cpu-little";
            operating-points-v2 = <&cluster0_opp>;
            vcpu-little-supply = <&mt6358_vproc12_reg>;
        };

        svs_cpu_big: svs-cpu-big {
            compatible = "mediatek,mt8183-svs-cpu-big";
            operating-points-v2 = <&cluster1_opp>;
            vcpu-big-supply = <&mt6358_vproc11_reg>;
        };

        svs_cci: svs-cci {
            compatible = "mediatek,mt8183-svs-cci";
            operating-points-v2 = <&cci_opp>;
            vcci-supply = <&mt6358_vproc12_reg>;
        };

        svs_gpu: svs-gpu {
            compatible = "mediatek,mt8183-svs-gpu";
            power-domains = <&scpsys MT8183_POWER_DOMAIN_MFG_2D>;
            operating-points-v2 = <&gpu_opp_table>;
            vgpu-spply = <&mt6358_vgpu_reg>;
        };
    };
